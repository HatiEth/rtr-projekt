#version 150

in vec3 colorValue0;
in vec4 colorValue1;

in vec2 vTexCoord;

out vec4 outColor;

uniform sampler2D skinTextureSampler;

void main() {
	vec4 texColor = texture2D(skinTextureSampler,vTexCoord);
	vec4 maxFilter = 
	vec4(	0.1,
			0.2,
			0.2,
			0.5 );
	//outColor = max(maxFilter,texColor)*vec4(1,1,1,0.7);
	outColor = texColor;
	
}