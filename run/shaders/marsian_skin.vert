#version 150 compatibility

in vec3 position;
in vec2 texCoord;
in vec3 normal;
in vec4 boneIndex;
in vec4 skinWeight;
in vec3 blendIndex;
in vec3 positionOffset0;
in vec3 normalOffset0;
in vec3 positionOffset1;
in vec3 normalOffset1;

out vec3 colorValue0;
out vec4 colorValue1;
out vec2 vTexCoord;

void main()
{
    gl_Position = gl_ModelViewProjectionMatrix*vec4( position, 1.0 );
	colorValue0 = normal;
	colorValue1 = skinWeight;
	vTexCoord = texCoord;
}