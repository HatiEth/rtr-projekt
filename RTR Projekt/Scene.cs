﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTR_Projekt
{
    abstract class Scene : IDisposable
    {
        abstract public void draw();
        abstract public void onResize(int width, int height);
        abstract public void init();
        abstract public void cleanUp();
        public void reinit()
        {
            cleanUp();
            init();
        }

        /// <summary>
        /// Inherited abstract function from IDisposable to implement in scenes
        /// </summary>
        abstract public void Dispose();
    }
}
