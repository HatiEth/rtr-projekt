﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace RTR_Projekt
{
    class Camera
    {
        
        private Matrix4 m_matrix;
        private Matrix4 m_perspective;

        public Matrix4 Matrix
        {
            get { return m_matrix; }
            protected set { m_matrix = value; }
        }
        private float m_aspectRatio;
        private Vector2 m_viewport;
        public Vector2 ViewPort
        {
            get { return m_viewport; }
            set { 
                m_viewport = value;
                m_aspectRatio = m_viewport.X / m_viewport.Y;
                onResize();
            }
        }

        public float AspectRatio
        {
            get { return m_aspectRatio; }
            protected set { m_aspectRatio = value; }

        }

        public Camera()
        {
            
        }

        public void update(float dt)
        {
            
        }


        public void onResize()
        {
            GL.Viewport(0, 0, (int)ViewPort.X, (int)ViewPort.Y);
            m_perspective = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, m_aspectRatio, 1, 64);
        }   
    }
}
