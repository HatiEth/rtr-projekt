﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;

namespace RTR_Projekt
{
    public partial class MainForm : Form
    {
        private bool loaded = false;
        private float m_updateInterval;
        public float UpdateInterval
        {
            get { return m_updateInterval; }
            protected set { m_updateInterval = value; }
        }
        private Camera m_camera;


        private MenuStrip menuStrip1;
        private ToolStripMenuItem optionsToolStripMenuItem;
        private ToolStripMenuItem scenesToolStripMenuItem;
        private ToolStripMenuItem basicSceneToolStripMenuItem;
        private ToolStripMenuItem displayMarsianToolStripMenuItem;
        private ToolStripMenuItem reinitializeSceneToolStripMenuItem;
        private ToolStripMenuItem alphaToolStripMenuItem;

        
        private Scene m_scene;
        
        
        public MainForm()
        {
            
            
            InitializeComponent();
            this.updateIntervalValueLabel.Text = ""+updateIntervalSlider.Value;
            this.UpdateInterval = updateIntervalSlider.Value / 1000.0f ;
        }

        
        private void tkControlMain_Load(object sender, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);

            GL.Enable(EnableCap.DepthTest);
            m_scene = new DisplayMarsian();
            m_scene.init();
            m_camera = new Camera();


            loaded = true;
            // resize call on opengl context
            tkControlMain_Resize(tkControlMain, EventArgs.Empty);
            // add automatic render loop
            Application.Idle += Application_Idle;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        private void tkControlMain_Paint(object sender, PaintEventArgs e)
        {
            Render();
        }

        private void tkControlMain_Resize(object sender, EventArgs e)
        {
            if (!loaded)
                return;

            OpenTK.GLControl c = sender as OpenTK.GLControl;

            if (c.ClientSize.Height == 0)
                c.ClientSize = new System.Drawing.Size(c.ClientSize.Width, 1);

            //Debug.WriteLine(c.Width + " , " + c.Height + " | " + Width + " , " + Height);

            float aspect_ratio = c.Width / (float)c.Height;


            m_camera.ViewPort = new Vector2(c.Width, c.Height);


            m_scene.onResize(c.Width,c.Height);

        }


        void Application_Idle(object sender, EventArgs e)
        {
            while (tkControlMain.IsIdle)
            {

                Render();
            }
        }


        void Render()
        {
            if (!loaded)
                return;

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);



            m_scene.draw();

            tkControlMain.SwapBuffers();
        }

        private void updateIntervalSlider_Scroll(object sender, EventArgs e)
        {
            this.updateIntervalValueLabel.Text = this.updateIntervalSlider.Value + "";
            this.UpdateInterval = updateIntervalSlider.Value / 1000.0f;
        }


        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.updateIntervalSlider = new System.Windows.Forms.TrackBar();
            this.updateIntervalDescLabel = new System.Windows.Forms.Label();
            this.updateIntervalValueLabel = new System.Windows.Forms.Label();
            this.updateIntervalUnitLabel = new System.Windows.Forms.Label();
            this.tkControlMain = new OpenTK.GLControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alphaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scenesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.basicSceneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayMarsianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reinitializeSceneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updateIntervalSlider)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tkControlMain);
            this.splitContainer1.Size = new System.Drawing.Size(784, 538);
            this.splitContainer1.SplitterDistance = 259;
            this.splitContainer1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.updateIntervalSlider);
            this.flowLayoutPanel1.Controls.Add(this.updateIntervalDescLabel);
            this.flowLayoutPanel1.Controls.Add(this.updateIntervalValueLabel);
            this.flowLayoutPanel1.Controls.Add(this.updateIntervalUnitLabel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(257, 536);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // updateIntervalSlider
            // 
            this.updateIntervalSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.SetFlowBreak(this.updateIntervalSlider, true);
            this.updateIntervalSlider.Location = new System.Drawing.Point(3, 3);
            this.updateIntervalSlider.Maximum = 100;
            this.updateIntervalSlider.Minimum = 1;
            this.updateIntervalSlider.Name = "updateIntervalSlider";
            this.updateIntervalSlider.Size = new System.Drawing.Size(251, 45);
            this.updateIntervalSlider.TabIndex = 0;
            this.updateIntervalSlider.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.updateIntervalSlider.Value = 16;
            this.updateIntervalSlider.Scroll += new System.EventHandler(this.updateIntervalSlider_Scroll);
            // 
            // updateIntervalDescLabel
            // 
            this.updateIntervalDescLabel.AutoSize = true;
            this.updateIntervalDescLabel.Location = new System.Drawing.Point(3, 51);
            this.updateIntervalDescLabel.Name = "updateIntervalDescLabel";
            this.updateIntervalDescLabel.Size = new System.Drawing.Size(61, 13);
            this.updateIntervalDescLabel.TabIndex = 2;
            this.updateIntervalDescLabel.Text = "Frametime: ";
            // 
            // updateIntervalValueLabel
            // 
            this.updateIntervalValueLabel.AutoSize = true;
            this.updateIntervalValueLabel.Location = new System.Drawing.Point(70, 51);
            this.updateIntervalValueLabel.Name = "updateIntervalValueLabel";
            this.updateIntervalValueLabel.Size = new System.Drawing.Size(101, 13);
            this.updateIntervalValueLabel.TabIndex = 1;
            this.updateIntervalValueLabel.Text = "updateIntervalLabel";
            // 
            // updateIntervalUnitLabel
            // 
            this.updateIntervalUnitLabel.AutoSize = true;
            this.updateIntervalUnitLabel.Location = new System.Drawing.Point(177, 51);
            this.updateIntervalUnitLabel.Name = "updateIntervalUnitLabel";
            this.updateIntervalUnitLabel.Size = new System.Drawing.Size(20, 13);
            this.updateIntervalUnitLabel.TabIndex = 3;
            this.updateIntervalUnitLabel.Text = "ms";
            // 
            // tkControlMain
            // 
            this.tkControlMain.BackColor = System.Drawing.Color.Black;
            this.tkControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tkControlMain.Location = new System.Drawing.Point(0, 0);
            this.tkControlMain.Name = "tkControlMain";
            this.tkControlMain.Size = new System.Drawing.Size(519, 536);
            this.tkControlMain.TabIndex = 0;
            this.tkControlMain.VSync = false;
            this.tkControlMain.Load += new System.EventHandler(this.tkControlMain_Load);
            this.tkControlMain.Paint += new System.Windows.Forms.PaintEventHandler(this.tkControlMain_Paint);
            this.tkControlMain.Resize += new System.EventHandler(this.tkControlMain_Resize);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.scenesToolStripMenuItem,
            this.reinitializeSceneToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alphaToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // alphaToolStripMenuItem
            // 
            this.alphaToolStripMenuItem.CheckOnClick = true;
            this.alphaToolStripMenuItem.Name = "alphaToolStripMenuItem";
            this.alphaToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.alphaToolStripMenuItem.Text = "Alpha blending";
            this.alphaToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.alphaToolStripMenuItem_CheckStateChanged);
            // 
            // scenesToolStripMenuItem
            // 
            this.scenesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.basicSceneToolStripMenuItem,
            this.displayMarsianToolStripMenuItem});
            this.scenesToolStripMenuItem.Name = "scenesToolStripMenuItem";
            this.scenesToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.scenesToolStripMenuItem.Text = "Scenes";
            // 
            // basicSceneToolStripMenuItem
            // 
            this.basicSceneToolStripMenuItem.Name = "basicSceneToolStripMenuItem";
            this.basicSceneToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.basicSceneToolStripMenuItem.Text = "BasicScene";
            this.basicSceneToolStripMenuItem.Click += new System.EventHandler(this.basicSceneToolStripMenuItem_Click);
            // 
            // displayMarsianToolStripMenuItem
            // 
            this.displayMarsianToolStripMenuItem.Name = "displayMarsianToolStripMenuItem";
            this.displayMarsianToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.displayMarsianToolStripMenuItem.Text = "DisplayMarsian";
            this.displayMarsianToolStripMenuItem.Click += new System.EventHandler(this.displayMarsianToolStripMenuItem_Click);
            // 
            // reinitializeSceneToolStripMenuItem
            // 
            this.reinitializeSceneToolStripMenuItem.Name = "reinitializeSceneToolStripMenuItem";
            this.reinitializeSceneToolStripMenuItem.Size = new System.Drawing.Size(109, 20);
            this.reinitializeSceneToolStripMenuItem.Text = "Reinitialize Scene";
            this.reinitializeSceneToolStripMenuItem.Click += new System.EventHandler(this.reinitializeSceneToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "RTR";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updateIntervalSlider)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private GLControl tkControlMain;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TrackBar updateIntervalSlider;
        private System.Windows.Forms.Label updateIntervalValueLabel;
        private System.Windows.Forms.Label updateIntervalDescLabel;
        private System.Windows.Forms.Label updateIntervalUnitLabel;

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void changedScene()
        {
            m_scene.onResize(tkControlMain.Width, tkControlMain.Height);
            m_scene.init();
        }

        private void basicSceneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_scene is BasicScene)
                return;
            m_scene.cleanUp();
            m_scene = new BasicScene(this);
            
            changedScene();
            
        }

        private void displayMarsianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_scene is DisplayMarsian)
                return;
            m_scene.cleanUp();
            m_scene = new DisplayMarsian();

            changedScene();
        }

        private void reinitializeSceneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_scene.reinit();
        }

        private void MainForm_Closing(object sender, FormClosingEventArgs e)
        {
            m_scene.cleanUp();
        }

        private void alphaToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            if (alphaToolStripMenuItem.Checked)
            {
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            }
            else
            {
                GL.Disable(EnableCap.Blend);
            }
            
        }

    }


}
