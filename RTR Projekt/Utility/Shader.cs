﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Diagnostics;


namespace RTR_Projekt.Utility
{
    class Shader
    {
        private ShaderType m_type;
        private int m_GLid;

        private string m_sourceFile;
        private bool m_compiled;

        public bool Compiled
        {
            get { return m_compiled; }
            protected set { m_compiled = value; }
        }

        public int ID
        {
            get { return m_GLid; }
            protected set { m_GLid = value; }
        }

        public Shader(ShaderType type, string shaderSourceFile)
        {
            m_compiled = false;
            m_type = type;
            m_sourceFile = shaderSourceFile;
        }

        public void compile()
        {
            // if already compiled - do recompile
            if (m_compiled || m_GLid != 0)
            {
                GL.DeleteShader(m_GLid);
            }
            string fileData = System.IO.File.ReadAllText(m_sourceFile);
            m_GLid = GL.CreateShader(m_type);

            GL.ShaderSource(m_GLid, fileData);
            GL.CompileShader(m_GLid);

            int infoLoglength = 0;
            GL.GetShader(m_GLid, ShaderParameter.InfoLogLength, out infoLoglength);
            Debug.WriteLineIf(infoLoglength > 1, GL.GetShaderInfoLog(m_GLid));
            Debug.WriteLineIf(infoLoglength <= 1, "Shader(" + m_GLid + "): Empty(Ok)");
            m_compiled = true;
        }

        ~Shader()
        {
        }
    }
}
