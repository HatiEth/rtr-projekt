﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Diagnostics;
using RTR_Projekt.Utility;



namespace RTR_Projekt
{
    class ShaderProgram
    {
        private int m_GLid;
        public int ID
        {
            get { return m_GLid; }
            protected set { m_GLid = value; }
        }

        public ShaderProgram()
        {
            ID = GL.CreateProgram();
        }

        public void link()
        {
            GL.LinkProgram(m_GLid);
            int logLength;
            GL.GetProgram(m_GLid, ProgramParameter.InfoLogLength, out logLength);

            Debug.WriteLineIf(logLength>1,GL.GetProgramInfoLog(m_GLid));
            Debug.WriteLineIf(logLength <= 1, "ShaderProgram(" + m_GLid + "): Empty(Ok)");
        }

        public void attach(Shader shader)
        {
            Debug.Assert(shader.ID != 0,"Internal GL Id error","Shader ID was zero, maybe shader not compiled");
            GL.AttachShader(m_GLid, shader.ID);
        }

        public void detach(Shader shader)
        {
            GL.DetachShader(m_GLid, shader.ID);
        }
        

        ~ShaderProgram()
        {
        }

        public void clear()
        {
            int shaderMaxCount;
            int shaderCount;

            GL.GetProgram(m_GLid, ProgramParameter.AttachedShaders, out shaderMaxCount);
            do
            {
                int shader;
                GL.GetAttachedShaders(m_GLid, shaderMaxCount, out shaderCount, out shader);
                GL.DetachShader(m_GLid, shader);
                GL.DeleteShader(shader);
            } while (shaderCount > 0);
            GL.DeleteProgram(m_GLid);
        }
    }
}
