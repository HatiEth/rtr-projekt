﻿using RTR_Projekt.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using System.Drawing;
using System.Drawing.Imaging;

namespace RTR_Projekt
{

    
    class DisplayMarsian : Scene
    {
        private int m_marsianVAO;
        private int[] m_marsianskin = new int[2];
        private int m_marsianTexture;

        Camera m_camera;
        
        private int m_renderCount;

        ShaderProgram m_displayProgram;  

        public DisplayMarsian()
        {
            m_camera = new Camera();
        }

        public override void draw()
        {

            Matrix4 lookat = Matrix4.LookAt(0, 0, 50, 0, 0, 0, 0, 1, 0);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref lookat);

            GL.UseProgram(m_displayProgram.ID);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, m_marsianTexture);
            GL.Uniform1(GL.GetUniformLocation(m_displayProgram.ID, "skinTextureSampler"), 0);
            
            GL.BindVertexArray(m_marsianVAO);
            
            GL.DrawElements(BeginMode.Triangles, 91932, DrawElementsType.UnsignedInt, 0);

            GL.BindVertexArray(0);

            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.UseProgram(0);
        }

        public override void onResize(int width, int height)
        {
            
            m_camera.ViewPort = new OpenTK.Vector2(width, height);

            Matrix4 perpective = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, m_camera.AspectRatio, 1, 64);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref perpective);
        }




        public override void init()
        {

            // Create Shader program

            m_displayProgram = new ShaderProgram();

            Shader s = new Shader(ShaderType.FragmentShader, "shaders/marsian_skin.frag");
            s.compile();
            m_displayProgram.attach(s);


            s = new Shader(ShaderType.VertexShader, "shaders/marsian_skin.vert");
            s.compile();
            m_displayProgram.attach(s);

            GL.BindFragDataLocation(m_displayProgram.ID, 0, "outColor");

            m_displayProgram.link();

            GL.UseProgram(0);

            /************************************************************************/
            /* Load marsian texture for mesh                                        */
            /************************************************************************/
            GL.Enable(EnableCap.Texture2D);
            GL.ActiveTexture(TextureUnit.Texture0);
            //GL.GenTextures(1, out m_marsianTexture);
            m_marsianTexture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, m_marsianTexture);

            System.Drawing.Bitmap textureHandler = new Bitmap("data/Texturen/SkinTex_Decal.jpg");
            BitmapData texData = textureHandler.LockBits(new Rectangle(0, 0, textureHandler.Width, textureHandler.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

           

            //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            GL.TexEnv(TextureEnvTarget.TextureEnv, TextureEnvParameter.TextureEnvMode, (int)TextureEnvMode.Modulate);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, texData.Width, texData.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgr, PixelType.UnsignedByte, texData.Scan0);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            //clear
            textureHandler.UnlockBits(texData);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            /************************************************************************/
            /* Create Vertex, Index buffers and vertex array for fast bind          */
            /************************************************************************/
            byte[] vboContent = System.IO.File.ReadAllBytes("data/Geometrie/MarsienneSkin_VB.raw");
            //float[] vboContent = {  0.0f,  0.5f, 0.0f,
            //                        0.5f, -0.5f, 0.0f,
            //                        -0.5f, -0.5f, 0.0f
            //                    };
            byte[] iboContent = System.IO.File.ReadAllBytes("data/Geometrie/MarsienneSkin_IB.raw");

            GL.GenVertexArrays(1, out m_marsianVAO);
            GL.BindVertexArray(m_marsianVAO); // Push marsianVbo Settings into marsianVAO

            GL.EnableClientState(ArrayCap.VertexArray);

            GL.GenBuffers(2, m_marsianskin);
            GL.BindBuffer(BufferTarget.ArrayBuffer, m_marsianskin[0]);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr) (vboContent.Length * sizeof(byte)), vboContent, BufferUsageHint.StaticDraw);

            /**
             * Datenlayout vboContent:
             *  Name            Type   <->  ShaderName      | Stride(B)=124 | Offset(B)
             *  Position        3float <-> position         |               |   00
             *  TexCoord        2float <-> texCoord         |               |   12
             *  Normal          3float <-> normal           |               |   20
             *  BoneIndex       4float <-> boneIndex        |               |   32
             *  Skin Weights    4float <-> skinWeight       |               |   48
             *  Blend Index     3float <-> blendIndex       |               |   64
             *  positionOffset0 3float <-> positionOffset0  |               |   76
             *  normalOffset0   3float <-> normalOffset0    |               |   88
             *  positionOffset1 3float <-> positionOffset1  |               |   100
             *  normalOffset1   3float <-> normalOffset1    |               |   112
             *  Stride: 31*sizeof(float) = 124
             **/
            /*
             *  in vec3 position;    
                in vec2 texCoord;
                in vec3 normal;
                in vec4 boneIndex;
                in vec4 skinWeight;
                in vec3 blendIndex;
                in vec3 positionOffset0;
                in vec3 normalOffset0;
                in vec3 positionOffset1;
                in vec3 normalOffset1;
             */
            int offset=0;
            int stride = 124;
            int dbg;
            GL.VertexAttribPointer(dbg=GL.GetAttribLocation(m_displayProgram.ID, "position")        , 3, VertexAttribPointerType.Float, false, stride, offset);
            GL.VertexAttribPointer(dbg=GL.GetAttribLocation(m_displayProgram.ID, "texCoord")        , 2, VertexAttribPointerType.Float, false, stride, offset+=3*sizeof(float));
            GL.VertexAttribPointer(dbg=GL.GetAttribLocation(m_displayProgram.ID, "normal")          , 3, VertexAttribPointerType.Float, false, stride, offset+=2*sizeof(float));
            GL.VertexAttribPointer(dbg=GL.GetAttribLocation(m_displayProgram.ID, "boneIndex")       , 4, VertexAttribPointerType.Float, false, stride, offset+=3*sizeof(float));
            GL.VertexAttribPointer(dbg=GL.GetAttribLocation(m_displayProgram.ID, "skinWeight")      , 4, VertexAttribPointerType.Float, false, stride, offset+=4*sizeof(float));
            GL.VertexAttribPointer(dbg=GL.GetAttribLocation(m_displayProgram.ID, "blendIndex")      , 3, VertexAttribPointerType.Float, false, stride, offset+=4*sizeof(float));
            GL.VertexAttribPointer(dbg=GL.GetAttribLocation(m_displayProgram.ID, "positionOffset0") , 3, VertexAttribPointerType.Float, false, stride, offset+=3*sizeof(float));
            GL.VertexAttribPointer(dbg=GL.GetAttribLocation(m_displayProgram.ID, "normalOffset0")   , 3, VertexAttribPointerType.Float, false, stride, offset+=3*sizeof(float));
            GL.VertexAttribPointer(dbg=GL.GetAttribLocation(m_displayProgram.ID, "positionOffset1") , 3, VertexAttribPointerType.Float, false, stride, offset+=3*sizeof(float));
            GL.VertexAttribPointer(dbg=GL.GetAttribLocation(m_displayProgram.ID, "normalOffset1")   , 3, VertexAttribPointerType.Float, false, stride, offset+=3*sizeof(float));


            // Enable VertexAttribArrays
            GL.EnableVertexAttribArray(GL.GetAttribLocation(m_displayProgram.ID, "position"));
            GL.EnableVertexAttribArray(GL.GetAttribLocation(m_displayProgram.ID, "texCoord"));
            GL.EnableVertexAttribArray(GL.GetAttribLocation(m_displayProgram.ID, "normal"));
            GL.EnableVertexAttribArray(GL.GetAttribLocation(m_displayProgram.ID, "boneIndex"));
            GL.EnableVertexAttribArray(GL.GetAttribLocation(m_displayProgram.ID, "skinWeight"));
            GL.EnableVertexAttribArray(GL.GetAttribLocation(m_displayProgram.ID, "blendIndex"));
            GL.EnableVertexAttribArray(GL.GetAttribLocation(m_displayProgram.ID, "positionOffset0"));
            GL.EnableVertexAttribArray(GL.GetAttribLocation(m_displayProgram.ID, "normalOffset0"));
            GL.EnableVertexAttribArray(GL.GetAttribLocation(m_displayProgram.ID, "positionOffset1"));
            GL.EnableVertexAttribArray(GL.GetAttribLocation(m_displayProgram.ID, "normalOffset1"));


            GL.BindBuffer(BufferTarget.ElementArrayBuffer, m_marsianskin[1]);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(sizeof(byte) * iboContent.Length), iboContent, BufferUsageHint.StaticDraw);
            
            
            // Unbind marsianVAO
            GL.BindVertexArray(0);
        }

        public override void cleanUp()
        {
            GL.UseProgram(0);

            m_displayProgram.clear();

            GL.DeleteBuffers(2, m_marsianskin);
            GL.DeleteVertexArrays(1, ref m_marsianVAO);
            //GL.DeleteTextures(1, ref m_marsianTexture);
            GL.DeleteTexture(m_marsianTexture);
        }


        public override void Dispose()
        {
            cleanUp();
        }
    }
}
